#!/usr/bin/env python3

import json
from tkinter import *
from tkinter import ttk
from PIL import ImageTk, Image
from tkcalendar import *

import json
import copy as cp

import Metadata_dict


def create_selectbox(j, element, second_frame):
    globals()[f"canvas_{j}"] = Canvas(second_frame, bd=0, highlightthickness=0)
    globals()[f"canvas_{j}"].pack(side="top")

    globals()[f"label_{j}"] = Label(globals()[f"canvas_{j}"], text=element['key'])
    globals()[f"label_{j}"].config(font=("Arial", 10))
    globals()[f"label_{j}"].grid(row=0, column=0, sticky=W, pady=2)

    globals()[f"var_{j}"] = StringVar()
    globals()[f"var_{j}"].set(element['default'])

    options = element['values']
    globals()[f"select_{j}"] = OptionMenu(globals()[f"canvas_{j}"], globals()[f"var_{j}"], *options)
    globals()[f"select_{j}"].grid(row=0, column=1, sticky=W, pady=2)

    globals()[f"abstand_{j}"] = Label(second_frame)
    globals()[f"abstand_{j}"].pack()

    var = globals()[f"var_{j}"]
    #Metadata_dict.add_to_meta_dict(element, var)

    return 0
