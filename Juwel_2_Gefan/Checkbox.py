#!/usr/bin/env python3

import json
from tkinter import *
from tkinter import ttk
from PIL import ImageTk,Image
from tkcalendar import *

import json
import copy as cp



def create_checkbox(j, element, second_frame):	
	
	globals()[f"canvas_{j}"] = Canvas(second_frame, bd=0, highlightthickness=0)
	globals()[f"canvas_{j}"].pack(side="top")
				
	
	globals()[f"label_{j}"] = Label(globals()[f"canvas_{j}"], text=element['key'])
	globals()[f"label_{j}"].config(font=("Arial", 10))
	globals()[f"label_{j}"].grid(row = 0, column = 0, sticky = W, pady = 2)
	
		
					
	for item in element['values']:
		k = element['values'].index(item)					
		globals()[f"var_{j}_{k}"] = StringVar()
		globals()[f"checkbox_{j}_{k}"] = Checkbutton(globals()[f"canvas_{j}"], text=item, variable=globals()[f"var_{j}_{k}"], onvalue=item, offvalue="none")
		globals()[f"checkbox_{j}_{k}"].deselect()
		globals()[f"checkbox_{j}_{k}"].grid(row = k, column = 1, sticky = W, pady = 2)


	
	globals()[f"abstand_{j}"] = Label(second_frame)
	globals()[f"abstand_{j}"].pack()
	
	return 0
