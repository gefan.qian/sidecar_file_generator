#!/usr/bin/env python3

import json
from tkinter import *
from tkinter import ttk
from PIL import ImageTk,Image
from tkcalendar import *


import copy as cp

import Selectbox
import Checkbox


###################################################################
# Funktion zum Metadaten speichern ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###################################################################

def save():

	print(Selectbox.var_1.get())
	print(Checkbox.var_2_0.get())
	print(Checkbox.var_2_1.get())
	print(Checkbox.var_2_2.get())
	print(Selectbox.var_3.get())
	
	


	return 0


###################################################################
# JSON-config-File öffnen und in einer Dictionary speichern ~~~~~~~
###################################################################

def json_transformer():
 
	# Opening JSON file
	f = open('configuration.json')
 
	# returns JSON object as a dictionary
	config = json.load(f)
	json_object = json.dumps(config, indent = 4)

	#print(json_object)	# Ausgabe der JSON/Dictionary zur Kontrolle


###################################################################
# Erstellen verschachtelter Frames, um die Scrollbar zu ermöglichen
###################################################################


	# Erstellen des Hauptfensters
	root = Tk()				
	root.title('Juwel - Metadaten-Tool')
	root.geometry("600x600")

	main_frame = Frame(root)
	main_frame.pack(fill=BOTH, expand=1)

	my_canvas = Canvas(main_frame, bd=0, highlightthickness=0)
	my_canvas.pack(side=LEFT, fill=BOTH, expand=1)

	my_scrollbar = ttk.Scrollbar(main_frame, orient=VERTICAL, command=my_canvas.yview)
	my_scrollbar.pack(side=RIGHT, fill=Y)

	my_canvas.configure(yscrollcommand=my_scrollbar.set)
	my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion = my_canvas.bbox("all")))

	second_frame = Frame(my_canvas, bd=0, highlightthickness=0)
	my_canvas.create_window((0,0), window=second_frame, anchor="n")


########################################################################################################
# Für die Datenspeicherung wird eine Dictionary erstellt ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
########################################################################################################
	
	#global metadata_dict
	#metadata_dict = {}


########################################################################################################
# Für jedes Element in der Dictionary namens config wird der Objekttyp geprüft und im Fenster realisiert
########################################################################################################
	
	
	for key in config:
		j = key
		element = config[key]
		if element["GUI_type"] == "selectbox":			
			Selectbox.create_selectbox(j, element, second_frame)

		
		elif element["GUI_type"] == "checkbox":			
			Checkbox.create_checkbox(j, element, second_frame)
			
			
		
			


			
				
					
	


	
########################################################################################################
# Metadaten speichern ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
########################################################################################################

	abstand_save = Label(second_frame)
	abstand_save.pack()
	myButton = Button(second_frame, text="Save the metadata", command=save).pack()


	
	root.mainloop()






