#!/usr/bin/env python3

import codecs
import json
import os

#from Juwel_2 import GUI_element_finder
import GUI_element_finder

from pathlib import Path


# Diese Funktion dient dazu, die manuellen Eintragungen in einer config-File
# zu parsen und in eine für das Juwel-Programm verständliche Json-Struktur
# umzuwandeln


class Text_parser:


    def __init__(self):
        home = str(Path.home())
        self.file_name = f'{home}/.juwel/config_template.txt'
        # Loads the default configuration file from a default path
        self.reset_all()

    def reset_all(self):
        self.f_string = "none"
        self.lines = []
        self.widget_raw_dict = {}
        self.positioning_list = []
        self.newline_dict = {}

        self.wrong_widget_dict = {}
        self.wrong_label_dict = {}
        self.wrong_default_dict = {}
        self.wrong_items_dict = {}
        self.wrong_format_dict = {}


        self.widget_dict = {}
        self.label_dict = {}
        self.default_dict = {}
        self.items_dict = {}
        self.format_dict = {}


        self.final_dict = {}
        self.final_list = []

        self.read_template()
        self.create_widget_raw()
        self.create_positioning_list()
        self.create_newline_dict()
        self.create_wrong_dicts()
        self.create_right_dicts()
        self.create_final_dict()
        self.correct_widget_names()
        self.create_final_list()
        self.add_default_and_format()

    def get_final_list(self):
        return self.final_list

    def change_path(self, different_path):
        self.file_name = different_path
        # If a different path (different configuration file) is chosen, the default path will be replaced by it
        self.reset_all()

    def read_template(self):
        my_encoding='utf-8'
        try:
            fh=codecs.open(self.file_name,'r',encoding=my_encoding)
        except OSError as e:
            self.final_list = [{"widget_type": "label", "labeltext": "Please select a configuration file (Menu -> Load configuration).          ", "newline": "no"}]
            # If no configuration file is selected, the GUI has no widgets. You first need to load a configuration file.
            return self.final_list
        f_str=fh.read().encode(my_encoding)
        fh.close()
        self.f_string = f_str.decode('utf-8')

    def create_widget_raw(self):
        self.lines = self.f_string.split('\n') # The content of the configfile gets splitted into each and every line
        widget_counter = 0
        inter_list = []
        for line in self.lines:
            # Every line of the configfile is regarded individually
            if line.strip() == 'layout':
                break   # If the parser has reached the layout block at the bottom of the configfile, it needs to stop,
                # or else the lines of the layout block will be
                # integrated into the widget dictionary and misunderstood as information about another widget.
                # Which would later lead to confusion for the parser
            elif line != '':
                inter_list.append(line)
                # If the line isn't blank, it will be added to an intermediate list
            elif line == '':
                # If it is a blank line, the program knows that the description of a widget has come to an end
                # and that the next widget description is about to begin
                self.widget_raw_dict[widget_counter] = inter_list
                # So the intermediate list, which contains the information about the last widget will be integrated
                # as a value into a widget dictionary, where the key of that value is a widget_counter
                inter_list = []
                # The intermediate list is emptied, so the lines of the next widget can be appended to it
                widget_counter = widget_counter + 1
                # The widget_counter, which serves as the keys from the widget dictionary will be increased to the next higher value

    def create_positioning_list(self):
        layout_bool = False
        for line in self.lines:
            # Again every line of the configfile is regarded individually
            line = line.strip()   # Also any spaces before and after the lines will be stripped away, so there's no confusion
            # about what string is read out of the line
            if line == 'layout':
                # If the line only contains the string "layout"
                layout_bool = True   # the program will know, the content isn't describing individual widgets anymore,
                # but it's describing the geometrical layout of those widgets
            if layout_bool == True:
                if len(line) > 0 and line[0].isdigit():
                    # This branch is explored, if the line is not blank and not containing anything else than digits
                    # (not for example a line with several spaces, so it looks like a blank line but isn't)
                    if len(line) > 1:
                        line = line.split()
                        # The purpose of this split is, if there are multiple digits in a line, to separate those digits,
                        # but still keeping them as individual elements together in one list
                    self.positioning_list.append(line)
                    # In the end you get a positioning_list, that keeps track of which widget numbers come in which order
                    # and if they belong to the same line (when they are together in one list) or in different lines
                else:
                    continue
                    # If it is a blank line, then it will be skipped (instead of the program crashing)

    def create_newline_dict(self):
        # This is a dictionary that collects all the newline-attributes ("yes" or "no") of each widget in the right order
        for element in self.positioning_list:
            if isinstance(element, str):
                self.newline_dict[element] = "yes"
                # If the element in positioning_list is a string (and therefore in this case a digit), that string/digit becomes the key of that
                # dictionary element and its value automatically is "yes", which stands for: "Yes, it's a new line"
            elif isinstance(element, list):
                # If the element is a list, then it means there are several widgets in one line
                for i in range(len(element)):
                    if i == 0:
                        self.newline_dict[element[i]] = "yes"
                        # For the first element in that line, the newline-attribute becomes "yes", because the first thing in a line logically
                        # always introduces a new line
                    elif i != 0:
                        self.newline_dict[element[i]] = "no"
                        # Every widget that is not in the first position of a line, is not introducing a new line, therefore its newline-attribute
                        # becomes "no"
                    # Remember that the digits (strings) represent widgets, they are the widgets' numbers and each of them become a key
                    # in the dictionary of collected newline-attributes (the latter are the values)

    def create_wrong_dicts(self):
         for key in self.widget_raw_dict:
            for element in self.widget_raw_dict[key]:
                # Every widget_raw_dict[key] is the conglomeration of information about one widget
                # and every element is one particular information or attribute (mostly key-value-pairs) about that widget
                element = element.strip()   # Just to be safe, any unnecessary space before and after that element will be stripped away
                if element[0].isdigit():
                    position_number = (element.split())[0]
                    widget_type = (element.split())[1]
                    self.wrong_widget_dict[position_number] = widget_type
                    # If the element starts with a number, it becomes the position number of that widget
                    # The consensus of the configfile is constructed in a way, so that the widget-type comes after its positioning number
                    # So in the self.wrong_self.widget_dict, which is a collection of all the widget-type attributes, the positioning numbers become the keys
                    # and the widget-types attributes become the values
                elif ":" in element:
                    # If the element doesn't start with a number, but contains a ":", it indicates that the element in question is a key-value-pair
                    pair = element.split(":")   # It needs to be split into the key (the expression before the ".")
                    # and the value (the expression after the ":"
                    if pair[0].strip() == "label" or pair[0].strip() == "key" or pair[0].strip() == "name":
                        self.wrong_label_dict[position_number] = pair[1].strip() + ": "
                    elif pair[0].strip() == "start" or pair[0].strip() == "default":
                        self.wrong_default_dict[position_number] = pair[1].strip()
                    elif pair[0].strip() == "items" or pair[0].strip() == "values" or pair[0].strip() =="options":
                        item_list = []
                        stripped_item_list = []
                        item_list = pair[1].split(",")
                        for item in item_list:
                            stripped_item_list.append(item.strip())
                        self.wrong_items_dict[position_number] = stripped_item_list
                    elif pair[0].strip() == "format":
                        self.wrong_format_dict[position_number] = pair[1].strip()                                    
                elif element == "layout":
                    break

    def create_right_dicts(self):
        # The following code block needs a little bit of a complex explanation for its purpose:
        # Up to now alle attributes have been saved in the order of how they are declared before the layout specifications
        # The real order however is the order of the layout specifications
        # That's why the previous order was wrong and needs to be corrected
        # As reference for the correction we use the newline_dict, of which they keys already are in the right order
        for key in self.newline_dict:
            if key in self.wrong_widget_dict:
                self.widget_dict[key] = self.wrong_widget_dict[key]
            if key in self.wrong_label_dict:
                self.label_dict[key] = self.wrong_label_dict[key]
            if key in self.wrong_default_dict:
                self.default_dict[key] = self.wrong_default_dict[key]
            if key in self.wrong_items_dict:
                self.items_dict[key] = self.wrong_items_dict[key]
            if key in self.wrong_format_dict:
                self.format_dict[key] = self.wrong_format_dict[key]

    def create_final_dict(self):
        # Now all attributes are categorized in different dictionaries.
        # However we need one big dictionary, where all indiviual widgets contain their clustered attributes
        # The attribute types (names of the dictionaries) should be the keys and the attribute values should be the values
        # whereas the former keys inside the dictionaries should be the identification numbers (outer keys) of the widgets (outer values)
        # The code implementation might be a bit twisted to read
        for key in self.widget_dict:
            inter_dict = {}
            inter_dict["widget_type"] = self.widget_dict[key]
            if key in self.label_dict:
                inter_dict["key"] = self.label_dict[key]
            if key in self.default_dict:
                inter_dict["default"] = self.default_dict[key]
            if key in self.items_dict:
                inter_dict["values"] = self.items_dict[key]
            if key in self.format_dict:
                inter_dict["format"] = self.format_dict[key]                                
            if key in self.newline_dict:
                inter_dict["newline"] = self.newline_dict[key]
            self.final_dict[key] = inter_dict

    def correct_widget_names(self):
        # Because often the user uses different words for the same widget type, any aberrant terms will be translated into default notations
        # for widget, so that the Juwel program understand which widget is meant
        for key in self.final_dict:
            corrected_widget_type = GUI_element_finder.find_gui_element(self.final_dict[key]["widget_type"])
            self.final_dict[key]["widget_type"] = corrected_widget_type

    def create_final_list(self):
        # We're almost at the end: Because we don't need the keys for the Juwel program to interpret the configfile
        # we might as well strip that dictionary of all its (outer) keys and leave the values
        # to turn it into a list (to reduce unnecessary information)        
        for key in self.final_dict:
            self.final_list.append(self.final_dict[key])


    def add_default_and_format(self):
        # One last correction: If no default is declared, selectboxes and expiration selectboxes use the first values of the option list
        # as default value
        # If for a date related widget (calendar or expiration-selectbox) no format is stated, the program automatically uses the German format
        for element in self.final_list:
            if "default" not in element or element["default"] == "":
                if element["widget_type"] == "selectbox" or element["widget_type"] == "expiration":
                    element["default"] = element["values"][0]
                elif element["widget_type"] == "textfield":
                    element["default"] = "Max Mustermann"                
            if element["widget_type"] == "calendar" or element["widget_type"] == "expiration":
                if "format" not in element or element["format"] == "":
                    element["format"] = "deutsch"
