# Content
1. [Introduction](#1-introduction)

   1.1 [What is the config.selection.json?](#11-what-is-the-configselectionjson)

   1.2 [JSON sample](#12-json-sample)

   1.3 [Common attributes](#13-common-attributes)

1. [Description for checkbox](#2-description-for-checkbox)

1. [Description for datefield](#3-description-for-datefield)

1. [Description for dropdown](#4-description-for-dropdown)

1. [Description for spinbox](#5-description-for-spinbox)

1. [Description for textfield](#6document-for-textfield)

   6.1 [Description for Email](#61-description-for-emails)


## 1.) Introduction
- This is a Technical description of how to write Configuration files. Those configuration filesare using normal JSON. Below is a more specific description of all possibilities.

- The configuration file needs to contain at least one JSON object. Every object represents one GUI field. Further the object name is going to be the displayable name. Every object needs a variable called "type": with one of the following as content "checkbox", "datefield", "dropdown", "spinbox", "textfield" or "Email".

- In case you have trouble to make a valid JSON read the warnings of the application or try out a JSON Formatter/Validator for example https://jsonformatter.curiousconcept.com/ ! This website can show you where ur exact problems are and sometimes even fix them for you.

### 1.1) What is the config.selection.json?
- The config.selection.json is the core file to decide which config file to load.It has variables inside with the datatype list

- For example: ` "key_name": ["datatype_object1", "datatype_object2", "pdf"] `

- The applications is reading this file and check if the given datatype is somewhere listed in this file. If it is successful it is going to take the key_name and load config.key_name.json. In case it failed the application has a fall back named config.default.json

### 1.2) JSON sample
- The order of Json object are important. The application will read from top to bottom, but the order of attributes are irrelevant.

Example:
```json
    {                                                   {
      "expire":{                                            "expire":{
      "value":"2020-12-02",          are equal              "type":"datefield"
      "type":"datefield"                                    "value":"2020-12-02",
   },                                                   },
```
### 1.3 Common attributes
- Most of the fields have common attributes this means that some attributes repeat in other objects with the same functionality. If nothing else is stated in their description this is valid.

**"value"** : Almost every type uses it mostly for default values. In most cases it can be empty.

**"type"**: Says what GUI element and which validator is going to be used to verify.

**"required_field"**: If it exist it can be either true or false. If it dosent exist the default value is True.

**"regex"** : Contains ONE valid regex which will be checked on validation.

**"execute"** : Contains ONE path to a script which will be executed. This Script needs to save a file in temp folder and named key_name.
temp.If everything is correct this application will read the content of this file and prefill text fields.

**"layout"** : This field is only needed if the GUI object should be in a new row. 
Currently thereare only two options with this field either its not existing then no new line is happening or the content of "layout": is "new_line" then the **CURRENT** object is displayed in a new row.


----------------------------------------------------------------------------------------------------
## 2.) Description for checkbox
```json
"someCheckbox":{
   "value":["yes", "no", "maybe", "blue", "red", "yellow", "orange", "black"],
   "default": ["red", "orange"],
   "single_answer": true,
   "type":"checkbox",
   "required_field":true
},

"favorite_color":{
   "value":["yes", "no", "maybe", "blue", "red", "yellow", "orange", "black"],
   "default": ["orange"],
   "single_answer": false,
   "type":"checkbox",
   "required_field":true
},
```


\*Keys marked with a star are required

\*\*Keys market with a double star are important

**\*value** : Must be a List of Strings which will be shown as options.

**default** : Must be either a List of strings or empty/not existing. Will automatically pre select fields (must be a subset of value).

**single_answer** : Must be either true or false. Default is falseOn true it is only possible to select exactly one 1 answer not more nor less. On false 0 or more field can be select.

**\*type** : "checkbox"

**\*\*regex** : This field do not have a regex option!

**Note: This field do not have any special validations.**


----------------------------------------------------------------------------------------------------

## 3.) Description for datefield
```json
"expire":{
   "value":"2020-12-02",
   "type":"datefield",
   "required_field":true,
}
```
\*Keys marked with a star are required

**\*value**\*: Must be either a valid datetime with format yyyy-mm-dd or empty string

**\*type**\* : "datefield"


----------------------------------------------------------------------------------------------------

## 4.) Description for dropdown
```json
"pizza_topics":{
   "value":[
      "salami",
      "tomato"
   ],
   "type":"dropdown",
}
```

\*Keys marked with a star are required

**\*value**: Must be a list of strings.
**\*type** : "datefield"


----------------------------------------------------------------------------------------------------

## 5.) Description for Spinbox
```json
"someSpinbox":{
   "min":"-10",
   "max":"126",
   "increment":"5",
   "default":"0",
   "type":"spinbox",
   "required_field":false
}
```

\*Keys marked with a star are required


**min** : Minimum value of the spinbox. Default min should be -Systemmax around ( -2^63 )

**max** : Maximum value of the spinbox. Default max should be Systemmax around ( 2^63-1 )

**increment** : Value of the factor that the spinbox gets incremented. Default is 1

**default** : Default value which the Spinbox has on Start. Default value is 0

**\*type** : "spinbox"

**Note: Floating values are allowed but keep in mind that the max and min number will be affected by this.**

**Systemmax is a variable from python the value can differ between computers! The value is depending on the Computer architecture. In worst case on a modern Computer the min and max value should be around 2 million.**


----------------------------------------------------------------------------------------------------

## 6.)Document for textfield
```json
"author":{
   "value":"",
   "type":"textfield",
   "required_field":true
}
```
**\*value**: Must be either a valid EMail or empty string.

**\*type** : "textfield"

**required_field**:  If it exist it can be either true or false.

**regex** : Contains ONE valid regex which will be checked on validation.

**execute**: same as above.

## 6.1.) Description for emails
- In a technical view Emails use the same technical background but a different validator!This results in that Textfield are not equal to E-Mail fields
```json
   "blue":{
      "value":"a@b.com",
      "type":"email",
      "required_field":false,
   }
```

**\*Keys** marked with a star are required

**\*value**: Must be either a valid EMail or empty string

**\*type** : "email"
   
**required_field**:  If it exist it can be either true or false

**regex** : This application already has a validator for a E-Mail address but other regex are allowed.
